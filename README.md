Quassel-Webserver Docker Image
==============================

Run the container with: `docker run k8r.eu/justjanne/quassel-webserver -p 64080:64080`

For configuration, you can mount a file at `/quassel-webserver/settings.user.js`:

```js
module.exports = {
    default: {  // Those can be overhidden in the browser
        host: '',  // quasselcore host
        port: 4242,  // quasselcore port
        initialBacklogLimit: 20,  // Amount of backlogs to fetch per buffer on connection
        backlogLimit: 100,  // Amount of backlogs to fetch per buffer after first retrieval
        securecore: true,  // Connect to the core using SSL
        theme: 'default',  // Default UI theme
        perchathistory: true,  // Separate history per buffer
        displayfullhostmask: false,  // Display full hostmask instead of just nicks in messages
        emptybufferonswitch: false,  // Trim buffer when switching to another buffer. Can be `false` or a positive integer
        highlightmode: 2  // Highlight mode: 1: None, 2: Current nick, 3: All nicks from identity
    },
    webserver: {
        socket: false,  // Tells the webserver to listen for connections on a local socket. This should be a path. Can be overhidden by '--socket' argument
        listen: null,  // Address on which to listen for connection, defaults to listening on all available IPs. Can be overhidden by '--listen' argument
        port: null,  // Port on which to listen for connection, defaults to 64080 for http mode, 64443 for https. Can be overhidden by '--port' argument
        mode: null  // can be 'http' or 'https', defaults to 'https'. Can be overhidden by '--mode' argument
    },
    themes: ['default', 'darksolarized'],  // Available themes
    forcedefault: false,  // Will force default host and port to be used if true, and will hide the corresponding fields in the UI.
    prefixpath: ''  // Configure this if you use a reverse proxy
};
```

To use this image with kubernetes, you should create the following resources. Make sure to use the
same (prefix-)path in the Ingress and the settings-user.js

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: quassel-webserver
  labels:
    app: quassel-webserver
data:
  settings-user.js: |-
    module.exports = {
        default: {  // Those can be overhidden in the browser
            host: '',  // quasselcore host
            port: 4242,  // quasselcore port
            initialBacklogLimit: 20,  // Amount of backlogs to fetch per buffer on connection
            backlogLimit: 100,  // Amount of backlogs to fetch per buffer after first retrieval
            securecore: true,  // Connect to the core using SSL
            theme: 'default',  // Default UI theme
            perchathistory: true,  // Separate history per buffer
            displayfullhostmask: false,  // Display full hostmask instead of just nicks in messages
            emptybufferonswitch: false,  // Trim buffer when switching to another buffer. Can be `false` or a positive integer
            highlightmode: 2  // Highlight mode: 1: None, 2: Current nick, 3: All nicks from identity
        },
        webserver: {
            socket: false,  // Tells the webserver to listen for connections on a local socket. This should be a path. Can be overhidden by '--socket' argument
            listen: null,  // Address on which to listen for connection, defaults to listening on all available IPs. Can be overhidden by '--listen' argument
            port: null,  // Port on which to listen for connection, defaults to 64080 for http mode, 64443 for https. Can be overhidden by '--port' argument
            mode: null  // can be 'http' or 'https', defaults to 'https'. Can be overhidden by '--mode' argument
        },
        themes: ['default', 'darksolarized'],  // Available themes
        forcedefault: false,  // Will force default host and port to be used if true, and will hide the corresponding fields in the UI.
        prefixpath: ''  // Configure this if you use a reverse proxy
    };
```

```yaml
kind: Deployment
apiVersion: extensions/v1beta1
metadata:
  name: quassel-webserver
  labels:
    app: quassel-webserver
spec:
  # Increase this if you need more replicas for scaling
  replicas: 1
  selector:
    matchLabels:
      app: quassel-webserver
  template:
    metadata:
      labels:
        app: quassel-webserver
    spec:
      volumes:
      - name: config
        configMap:
          name: quassel-webserver
      containers:
      - name: quassel-webserver
        image: k8r.eu/justjanne/quassel-webserver:2.2.8
        ports:
        - name: http
          containerPort: 80
          protocol: TCP
        volumeMounts:
        - name: config
          mountPath: /quassel-webserver/settings-user.js
          subPath: settings-user.js
```

```yaml
kind: Service
apiVersion: v1
metadata:
  name: quassel-webserver
  labels:
    app: quassel-webserver
spec:
  selector:
    app: quassel-webserver
  ports:
  - name: http
    protocol: TCP
    port: 80
    targetPort: http
  type: ClusterIP
```

```yaml
kind: Ingress
apiVersion: extensions/v1beta1
metadata:
  name: quassel-webserver
  labels:
    app: quassel-webserver
  annotations:
    kubernetes.io/ingress.class: nginx
    # Remove this if you don’t want Let’s Encrypt support
    kubernetes.io/tls-acme: true
spec:
  rules:
  # Change the host to your own
  - host: quassel.example.com
    http:
      paths:
      # Change the path to whatever you set in settings-user.js for prefixpath
      - path: /chat
        backend:
          serviceName: quassel-webserver
          servicePort: http
  # Remove this part if you don’t want Let’s Encrypt support
  tls:
  - secretName: quassel-webserver-tls
    hosts:
    # Change the host to your own
    - quassel.example.com
```
