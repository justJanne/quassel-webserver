FROM node:8-alpine as builder
RUN apk add --update git python alpine-sdk && \
    git clone https://github.com/magne4000/quassel-webserver.git /quassel-webserver && \
    cd /quassel-webserver && \
    git checkout tags/2.2.8 && \
    npm install --production

FROM node:alpine
RUN apk --no-cache add ca-certificates
COPY --from=builder /quassel-webserver /quassel-webserver

ENV NODE_ENV=production
WORKDIR /quassel-webserver

EXPOSE 64080

ENTRYPOINT ["node", "app.js", "-m", "http"]
